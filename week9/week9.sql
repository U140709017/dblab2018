select count(customers.CustomerID) as numberOfCustomers, customers.Country
from customers
group by customers.Country
order by numberOfCustomers DESC;

select count(products.ProductID) as numberOfProducts, suppliers.SupplierName
from products join suppliers on products.ProductID = suppliers.SupplierID
group by suppliers.SupplierID
order by numberOfProducts DESC

select * from customers;

show variables like '__secure_file_priv';

load data local infile 'E:\\Downloads\\Yeni\\customers.csv' 
into table customers
fields terminated by ';'
ignore 1 lines;

load data local infile 'E:\\Downloads\\yeni\\employees.csv' 
into table employees
fields terminated by ','
ignore 1 lines;

load data local infile 'E:\\Downloads\\yeni\\categories.tsv' 
into table categories;

select * from employees;
select * from categories;
