show columns from customers;

select * from customers
order by CustomerName;

describe customers;

create index index1
on customers(customerName);

explain select * from customers;

alter table customer drop index index1;

create view tmp as select * from customers
order by CustomerName;

select * from tmp;

insert into tmp values (100000,'a','a','a','a','a','a');
select * from tmp;