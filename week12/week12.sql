create table `employees_backup` (
	`id` int auto_increment,
    `employeeID` int(11) not null,
    `LastName` varchar(45) not null,
    `FirstName` varchar(45) not null,
    `BirthDate` varchar(45) not null,
    `changedat` datetime default null,
    `action` varchar(45) default null,
    primary key (`id`)
    );
    
select * from employees_backup;

update employees
set LastName = "Kahramanogluu"
where EmployeeID = 6;

select * from employees;
select * from employees_backup;

update employees
set BirthDate = '1999-01-01'
where EmployeeID = 6 ;

insert into employees(EmployeeID,LastName,FirstName,BirthDate,Photo,Notes)
	values ("123","Kahramanoglu","Ozan","1990-01-01","Photo","Intern")